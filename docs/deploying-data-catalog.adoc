[id="deploying-data-catalog"]

= Deploying Data Catalog Setup

This procedure describes how to enable (and disable) the Data Catalog for the Open Data Hub. The Data Catalog is a set of components with which you can run Data Exploration on your Data Lake. These components are:

* *Hive Metastore* to store metadada information about the Hive tables
* *Spark SQL Thrift server* to expose a ODBC/JDBC endpoint to interact with the Hive Tables
* *Hue* to connect to Spark SQL Thrift server and run queries, as well as create dashboards.

The diagram below shows how these components compose Data Catalog architecture:

[#data-catalog-arch]
[caption="Figure 1: ",link=images/data-catalog-architecture.jpg]
image::images/data-catalog-architecture.jpg[Sunset,1024,768]

== Prerequisites

* A terminal shell with the OpenShift client command `oc` availabe.
* A copy of the files in this repository available in your terminal shell.
* The ability to run the operator as outlined at link:manual-installation.adoc[manual-installation]

=== S3 Access

In order to use the Data Catalog, you must have access to a suitable S3 deployment. The Data Catalog is known to work with Amazon S3 as well as standalone deployments of Ceph S3. Support for Rook Ceph S3 instances is not yet enabled.

The URL that you use to access S3 must be of the format s3.$DOMAIN.$TLD, e.g. s3.example.com. Additionally, you must have access to a set of S3 credentials that have read and write access to an S3 bucket. Make note of the S3 endpoint URL, the bucket name, and the access key and secret key. These values will all be needed later on.

The S3 instance must have a secured (https) interface.

=== Test Data

You will need some test data suitable for loading into the data catalog. We recommend that you upload a suitable CSV file to your S3 endpoint/bucket. The first line of the CSV file should be a list of header names (take care that there are no special characters in the header field names). The file should be uploaded to S3 with a key prefix that places it inside of a "folder" in s3. That is to say that the file should not be uploaded to the root of the bucket. A suitable S3 URI would be s3://s3.exmple.com/mybucket/path/to/file.csv

== Tested configurations

. AWS S3
. Standalone Ceph over HTTPS

== Deployment Procedure

. Change directory to your copy of the repository.
. Login as cluster-admin
. Run the manual-installation steps 1 - 6 as outlined in this link:manual-installation.adoc[manual-installation]
. Copy the Open Data Hub custom resource to a place you will make edits `cp deploy/crds/opendatahub_v1alpha1_opendatahub_cr.yaml my_environment_cr.yaml`
. Open `my_environment_cr.yaml` in your editor of choice.
. Locate the `data-catalog` entry in the new file.
. Setting `odh_deploy` to `true` or `false` will either enable or disable Data Catalog, and setting `aws_access_key_id`, `aws_secret_access_key` and `s3_endpoint` will configure Hue and Thrift server to access S3. You will also want to specify appropriate values for the Hue and Thriftserver database credentials and secrets. Edit the following lines in the file to do so.

....
  data-catalog:
    odh_deploy: true
    aws_access_key_id: <your-aws-access-key-id>
    aws_secret_access_key: <your-aws-secret-access-key>
    s3_endpoint: s3.foo.com
    s3_is_secure: true

    hive-metastore:
      database:
        username: <change to a secure value>
        password: <change to a secure value>

    hue:
      hue_secret_key: <change to a secure value>
      database:
        username: <change to a secure value>
        password: <change to a secure value>
        root_password: <change to a secure value>
....
. The data catalog requires the Spark operator to run. Set the `odh_deploy` value to True for the Spark Operator as described link:here[../roles/spark-operator/README.md].
. Initiate the deployment with `oc apply -f my_environment_cr.yaml`

== Verification steps

To verify installation is successful:

* Check to see that all Data Catalog pods are running without errors.
* Open Hue and create the admin user. The first page will guide you through the process.
  . To locate the Hue URL, use the comand `oc get route/hue --template={{.spec.host}}`. Alternatively, click the URL associated with the Hue deployment in the OpenShift console UI.
* Check in the `S3` tab on the left bar if Hue lists all buckets from S3
* Check in the `SQL` tab on left bar if you Hue lists a default database
* Open the Spark master UI and check in the `Running applications` list there is an app called `SparkSQL::<service-ip-address>`
  . You must expose the service before access Spark Master UI. Run the command `oc expose svc/spark-cluster-data-catalog-ui` to expose it first.
  . After exposing the service, run the command `oc get route/spark-cluster-data-catalog-ui --template={{.spec.host}}` to get the Spark Master URL

== Advanced Verification

Assuming you have access to a csv file stored in an S3 bucket, you may perform the following advanced verification steps:

* Access the Hue editer interface. Create and use a database by running the following commands:

....
CREATE DATABASE testing
USE testing
....

* Create a table pointing to your test data. The below command is for sample purposes only. The list of columns will need to be altered to match your test data. You will need to update the LOCATION option to point to your test data file in S3. If your file is located at `s3://s3.example.com/path/to/data.csv` then the LOCATION option should be `s3://s3.exmple.com/path/to`.

....
CREATE EXTERNAL TABLE testing.sample(
    timestamp TIMESTAMP,
    name STRING,
    field STRING,
    primary_audience STRING,
    key_people STRING,
    outcome STRING,
    full_notes STRING,
    email STRING,
    sample_date DATE,
    notes STRING,
    lowlights STRING,
    learnings STRING,
    trip_region STRING,
    number_of_days INT,
    estimated_cost FLOAT,
    product_mix STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
"separatorChar" = ",",
"quoteChar" = "\"",
"escapeChar" = "\\"
)
TBLPROPERTIES("skip.header.line.count"="1")
LOCATION 's3a://<csv-file-location>'
....

* Run a query to select data from the newly created table:

....
select * from testing.sample limit 10;
....

== Additional resources

* More information about *Hive Metastore* can be found  at link:https://jaceklaskowski.gitbooks.io/mastering-spark-sql/spark-sql-hive-metastore.html[jaceklaskowski.gitbooks.io/mastering-spark-sql/spark-sql-hive-metastore.html].
* More information about *Spark SQL Thrift Server* can be found  at link:https://jaceklaskowski.gitbooks.io/mastering-spark-sql/spark-sql-thrift-server.html[jaceklaskowski.gitbooks.io/mastering-spark-sql/spark-sql-thrift-server.html].
* More information about *Hue* can be found  at link:http://www.gethue.com[www.gethue.com].

