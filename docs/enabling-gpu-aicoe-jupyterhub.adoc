
// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="enabling-gpu-aicoe-jupyterhub"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Enabling a GPU for use with AICoE JupyterHub
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

_AICoE-JupyterHub_ component has support for accessing NVIDIA GPUs from Jupyter notebooks. Currently,
we have verified that JupyterHub can successfully access the GPU in OpenShift 3.11 and OCP 4.x clusters.

.Prerequisites

* OCP cluster with GPU(s) enabled
** Enabling GPUs in OpenShift 3.11 is outside of the scope of Open Data Hub. There is an unofficial document by Zvonko Kaiser for OpenShift 3.11 on link:https://github.com/zvonkok/origin-ci-gpu/blob/release-3.11/doc/How%20to%20use%20GPUs%20with%20DevicePlugin%20in%20OpenShift%203.11%20.pdf[How to use GPUs with DevicePlugin in OpenShift 3.11] that can provide guidance.
** Enabling GPUs in OpenShift 4.x can be achieved by deploying link:https://github.com/zvonkok/node-feature-discovery[Cluster Node Feature Discovery Operator] and link:https://github.com/zvonkok/special-resource-operator[Special Resource Operator]

    1. Deploy NFD from OpenShift Operator Catalog    

    2. Deploy SRO (will be part of OperatorHub in the future)

    
    git clone https://github.com/openshift-psap/special-resource-operator
    git checkout release-4.2
    make -C special-resource-operator deploy

Once both NFD and SRO operators are running, check your GPU node resource manifest and look for `nvidia.com/gpu` in `.status.allocatable` in the output of the following command (replace `$GPU_NODE` by your GPU node name).

```
oc get node -o yaml $GPU_NODE
```
    

.Procedure

AICoE-JupyterHub component needs to make sure it can properly configure JupyterHub server to spawn user pods with correct setup to get access to GPUs. It expects the configuration in `gpu_mode` field and recognizes 3 values:

* empty/`null`
* `selinux`
* `privileged`

In OCP 4.x:

* Leave the `gpu_mode` empty or set to `null`

In OpenShift 3.11:

* If the configuration of the cluster is based on the documentation linked above, set `gpu_mode` to value `selinux`: 

```
aicoe-jupyterhub:
    gpu_mode: "selinux"
```

* In case the cluster GPU enablement in the cluster is configured differently it is probable containers require to run in `privileged` mode to gain access to GPUs. In such case set `gpu_mode` to value `privileged`

```
aicoe-jupyterhub:
    gpu_mode: "privileged"
```

In addition to making sure the containers can access the GPUs, there need to be compatible Jupyter notebook images built in the cluster. This is enabled by setting `deploy_cuda_notebooks` to `True`:

```
aicoe-jupyterhub:
    notebook_images:
        deploy_cuda_notebooks: True
```

This will deploy a set of `BuildConfigs` which prepare *CUDA enabled s2i Python images* which are then used for building `s2i-tensorflow-notebook-gpu` image available in JupyterHub Spawner UI. This image has Tensorflow GPU preinstalled and enables users to leverage GPUs available in cluster with Tensorflow library.

NOTE: The build process takes a lot of time due to the size of images and software which is being installed. You might need to wait 1-2 hours before the final build (`s2i-tensorflow-notebook-gpu`) is finished and available.

```
oc describe buildconfig s2i-tensorflow-notebook-gpu
```

If you are using the `s2i-tensorflow-notebook-gpu` and set `GPU` in JupyterHub Spawner UI to a number bigger than 0,you can verify that the GPU is actually available to TF by running following code in the Jupyter Notebook

```
import tensorflow as tf
tf.test.is_gpu_available()
```

The method `is_gpu_available()` should return `True`.

.Updating Cuda Build Chain template

The build chain template upstream can be found at https://github.com/thoth-station/tensorflow-build-s2i/tree/master/cuda

To update the link:../roles/aicoe-jupyterhub/templates/jupyter-gpu-build-chain/cuda-build-chain.json.j2[CUDA template] run the following command:

```
curl -o /tmp/cuda-build-chain.json https://raw.githubusercontent.com/thoth-station/tensorflow-build-s2i/master/cuda/cuda-build-chain.json
python scripts/process_cuda_template.py /tmp/cuda-build-chain.json > roles/aicoe-jupyterhub/templates/jupyter-gpu-build-chain/cuda-build-chain.json.j2
```

The `process_cuda_template.py` script turns the upstream JSON template into operator compatible Jinja template. 


//.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.

.Additional resources

* link:https://blog.openshift.com/how-to-use-gpus-with-deviceplugin-in-openshift-3-10[How to use GPUs with DevicePlugin in OpenShift 3.10]
* link:https://docs.openshift.com/container-platform/3.11/dev_guide/device_plugins.html[OpenShift 3.11 - Using Device Plug-ins]
* link:https://kubernetes.io/docs/tasks/manage-gpus/scheduling-gpus[Kubernetes GPU Documentation]
* link:https://docs.openshift.com/container-platform/3.11/admin_guide/manage_scc.html[OpenShift 3.11 Cluster Administration / Managing Security Context Constraints]
* link:https://github.com/AICoE/jupyterhub-ocp-oauth[JupyterHub deployment using OpenShift OAuth authenticator]
