Open Data Hub
=============

The Open Data Hub role is responsible for managing the intialization of configuration paramters of the other roles that are deployed by the operator. This role should not implement any tasks deploying components of the Open Data Hub.

Requirements
------------

No requirements other than it is expected that this is running on an official release of the [Ansible Operator](https://quay.io/repository/operator-framework/ansible-operator) image

Role Variables
--------------
`odh_spec` - This variable is set in the `opendatahub` task as a shortcut to the ODH custom resource `.spec` without having to account for [ansible variable name mangling](https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/dev/developer_guide.md#extra-vars-sent-to-ansible).

`opendatahub` - We use this keyname in the root of the ODH custom resource `.spec` to reference variable that are specific to the Open Data Hub role

Variables under opendatahub:
- `debug` - This variable is used a reference to enable any debug operations during ODH deployment. Currently, it's used to print out the ansible `hostvars` during the playbook execution

Dependencies
------------

None

Sample Configuration
--------------------

````
  opendatahub:
    debug: false
````

License
-------

GNU GPLv3

Author Information
------------------

contributors@lists.opendatahub.io
